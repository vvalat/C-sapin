#include "struct.h"

/* Fonction principale appelée par la main */
void	sapin(int size)
{
  ret_s	my_return; /* structure d'accueil de sortie de fonction */
  ret_s	*ret_ptr;
  int	ligne_tot;
  int	star_nbr;

  ret_ptr = &my_return;
  gen_foil_w(size, ret_ptr);
  ligne_tot = size * (4 + my_return.line_) / 2; /* somme arithmétique du nombre de ligne */
  my_return.ligne_tot_ = ligne_tot; /* Stockage pour appel dans draw_tree */
  draw_tree(ret_ptr);
  draw_trunk(size, ret_ptr);
}

/* Fonction de dessin du feuillage */
int	draw_tree(ret_s *ret_ptr)
{
  int	line;
  int	star_nbr;
  int	i;
  int	j;

  line = 1;
  i = 0;
  j = 0;
  while (line <= ret_ptr->ligne_tot_)
    {
      star_nbr = count_stars(line);
      while (j < (ret_ptr->foil_w_ - star_nbr) / 2 )
	{
	  my_putchar(' ');
	  j = j + 1;
	}
      while (i < star_nbr)
	{
	  my_putchar('*');
	  i = i + 1;
	}
      my_putchar('\n');
      i = 0;
      j = 0;
      line = line + 1;
    }
  return (0);
}

/* Fonction de dessin du tronc */
int	draw_trunk(int size, ret_s *ret_ptr)
{
  int	trunk_w;
  int	i;
  int	j;
  int	k;
  
  i = 0;
  j = 0;
  k = 0;
  trunk_w = 1;
  while (i < size) /* La hauteur du tronc est égale au nombre de structures du feuillage */
    {
      trunk_w = count_trunk(size);
      while (j < (ret_ptr->foil_w_ - trunk_w) / 2 )
	{
	  my_putchar(' ');
	  j = j + 1;
	}
      while (k < trunk_w)
	{
	  my_putchar('|');
	  k = k + 1;
	}
      my_putchar('\n');
      i = i + 1;
      j = 0;
      k = 0;
    }
  return (0);
}
