#ifndef STRUCT_H_
#define STRUCT_H_

struct return_structure
{
  int	foil_w_;
  int	line_;
  int	ligne_tot_;
};
typedef struct return_structure ret_s;

int	main(int argc, char *argv[]);
void	sapin(int size);
int	draw_tree(ret_s *ret_ptr);
int	draw_trunk(int size, ret_s *ret_ptr);
int	gen_foil_w(int size, ret_s *ret_ptr);
int	count_stars(int asked_line);
int	count_trunk(int size);


#endif
