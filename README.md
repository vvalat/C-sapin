Un sapin en C
===================


Voici un programme écrit en C codé en deux jours en clôture de la piscine  dédiée à ce langage durant la formation Coding Academy de l'Epitech Paris.

----------
Vous pouvez lancer l'éxécutable 'test' (Linux) ou compiler le programme avec gcc.

Le programme prend un argument de type entier désignant la largeur du tronc du sapin que vous souhaitez générer.

    > ./test 1
    
    >     *
         ***
        *****
       *******
          |
