#include <unistd.h>
#include "struct.h"

void	my_putchar(char c)
{
  write(1, &c, 1);
}

/* Fonction de calcul de la plus grande largeur du feuillage*/
int	gen_foil_w(int size, ret_s *ret_ptr)
{
  int	line;
  int	decalage; /* décalage du nombre d'étoile d'un étage à l'autre (une fois sur deux) */
  int	foil_w;
  int	i;
    
  foil_w = 7; /* initialisation pour le sapin minimal */
  line = 4;
  decalage = 2;
  i = 0;
  while (size > 1)
    {
      if (i == 2)
	{
	  i = 0;
	  decalage = decalage + 2;
	}
      line = line + 1;
      foil_w = (foil_w - decalage) + ((line - 1) * 2);
      i = i + 1;
      size = size - 1;
    }
  ret_ptr->foil_w_ = foil_w;
  ret_ptr->line_ = line;
  return (0);
}

/* Fonction de comptage du nombre d'étoiles par ligne */
int	count_stars(int asked_line)
{
  int	star_nbr;
  int	line;
  int	decalage;
  int	i;
  int	j;

  star_nbr = 1;
  line = 1;
  decalage = 2;
  i = 1;
  j = 0;
  while (line < asked_line)
    {
      if (line == (i * (4 + (4 + (i - 1))) / 2))
	{	  
	  if (j == 2)
	    {
	      decalage = decalage + 2;
	      j = 0;
	    }
	  star_nbr = star_nbr - decalage;
	  i = i + 1;
	  j = j + 1;
	} 
      else
	{
	  star_nbr = star_nbr + 2;
	}
      line = line + 1;
    }
  return (star_nbr);
}

/* Fonction d'évaluation de largeur de tronc */
int	count_trunk(int size)
{
  int	i;
  int	j;
  int	trunk_w;

  i = 0;
  j = 0;
  if (size == 1)
    {
      return (1);
    }
  trunk_w = 3;
  while (i < size - 1)
    {
      if (j == 2)
	{
	  trunk_w = trunk_w + 2;
	  j = 0;
	}
      i = i + 1;
      j = j + 1;
    }
  return (trunk_w);
}
